package com.sudip.eggtimer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int time;
    SeekBar timerSeekBar;
    boolean timerState=true;
    CountDownTimer timer;
    TextView timerText;
    public void setTimerText(int timeRemaining){
        int minute = (int)timeRemaining/60;
        int seconds = timeRemaining%60;
        String timeToDisplay = String.format("%02d:%02d",minute,seconds);
        timerText.setText(timeToDisplay);
    }
    public void handleButtonClick(View view)
    {
        timerState = (!timerState);
        if (!timerState)
        {
            timerSeekBar.setEnabled(false);
            timer = new CountDownTimer(time*1000,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    Log.i("info",Integer.toString(time));
                    setTimerText((int)millisUntilFinished/1000);
                }

                @Override
                public void onFinish() {
                    setTimerText(0);
                    MediaPlayer endSound = MediaPlayer.create(getApplicationContext(),R.raw.endsound);
                    endSound.start();
                    timerSeekBar.setEnabled(true);
                    view.setEnabled(true);
                }
            };
            timer.start();
            view.setEnabled(false);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timerSeekBar = (SeekBar) findViewById(R.id.seekBar);
        timerSeekBar.setMax(600);
        timerSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                time = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        timerText = (TextView) findViewById(R.id.textView);

    }
}